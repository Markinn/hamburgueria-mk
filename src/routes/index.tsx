import { Switch, Route } from "react-router-dom";
import { Dashboard } from "../pages/Dashboard";

const Router = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Dashboard />
      </Route>
    </Switch>
  );
};

export default Router;
