import { useState } from "react";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
interface propsStyle {
  open: boolean;
}

const PositionLogoDiv = styled.div`
  display: flex;
  align-items: center;
  h1 {
    cursor: pointer;
    font-size: 1.5rem;
    margin-left: 3px;
    color: var(--white);
    font-family: var(--font-roboto-mono);
  }
`;

const StyledShortDiv = styled.div`
  width: 7px;
  height: 50px;
  background-color: var(--white);
  border-radius: 15px;
  margin: 0 4px;
  @media (min-width: 768px) {
    width: 10px;
  }
`;

const StyledBigDiv = styled.div`
  width: 7px;
  height: 75px;
  background-color: var(--white);
  border-radius: 15px;
  margin: 0 4px;
  @media (min-width: 768px) {
    width: 10px;
  }
`;

const StyledHeader = styled.header`
  background-color: var(--red);
  height: 100px;
  width: 100%;
  display: flex;
  justify-content: space-around;
  align-items: center;
  position: relative;
  z-index: 100;
`;

const StyledBurger = styled.div<propsStyle>`
  width: 2rem;
  height: 2rem;
  position: relative;
  top: 15px;
  right: 20px;
  z-index: 20;
  display: none;
  margin-bottom: 1rem;

  @media (max-width: 768px) {
    display: flex;
    justify-content: space-around;
    flex-flow: column nowrap;
    top: 10px;
    right: 0;
  }

  div {
    width: 2rem;
    height: 0.25rem;
    background-color: var(--white);
    border-radius: 10px;
    transform-origin: 1px;
    transition: all 0.4s linear;

    &:nth-child(1) {
      transform: ${({ open }) => (open ? "rotate(45deg)" : "rotate(0)")};
    }

    &:nth-child(2) {
      transform: ${({ open }) => (open ? "translateX(100%)" : "translateX(0)")};
      opacity: ${({ open }) => (open ? 0 : 1)};
    }

    &:nth-child(3) {
      transform: ${({ open }) => (open ? "rotate(-45deg)" : "rotate(0)")};
    }
  }
`;
const StyledMenu = styled.ul<propsStyle>`
  list-style: none;
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  padding-top: 0;
  height: 100px;
  width: 440px;

  li {
    margin: 0 2.5rem;
    display: flex;
    gap: 7px;
    color: var(--black);

    p {
      color: var(--white);
      font-family: var(--font-roboto-mono);
      font-weight: bold;
      cursor: pointer;
    }
    i {
      cursor: pointer;

      background-color: var(--blue);
      width: 50px;
      height: 50px;
      border-radius: 100%;

      display: flex;
      align-items: center;
      justify-content: center;

      color: var(--white);
      font-weight: bold;
      font-size: 1.8rem;
    }

    &:hover {
      border-bottom: 3px solid #8c0d02;
    }

    @media (max-width: 768px) {
      font-size: 1.5rem;
      gap: 20px;
    }
  }
  @media (max-width: 768px) {
    flex-flow: column nowrap;
    position: fixed;
    transform: ${({ open }) => (open ? "translateX(0)" : "translateX(100%)")};
    top: 0;
    right: 0;
    height: 100vh;
    width: 250px;
    padding-top: 6.5rem;
    transition: transform 0.3s ease-in-out;
    background-color: ${({ open }) => (open ? "#000000" : "none")};

    display: flex;
    align-items: center;

    li {
      color: #fff;
      margin-right: 1.5rem;
      margin-bottom: 4rem;
      p {
        font-size: 2rem;
      }
    }
  }
`;

export const Header = () => {
  const history = useHistory();
  const [open, setOpen] = useState(false);

  return (
    <StyledHeader>
      <PositionLogoDiv>
        <StyledShortDiv />
        <StyledShortDiv />
        <StyledBigDiv />
        <StyledShortDiv />
        <StyledBigDiv />
        <h1 onClick={() => history.push("/")}>MK Burg's</h1>
      </PositionLogoDiv>

      <StyledBurger open={open} onClick={() => setOpen(!open)}>
        <div></div>
        <div></div>
        <div></div>
      </StyledBurger>

      <StyledMenu open={open}>
        <li onClick={() => history.push("/menu")}>
          <p>Cardápio</p>
        </li>
        <li onClick={() => history.push("/cart")}>
          <p>Carrinho</p>
        </li>
        <li onClick={() => history.push("/login")}>
          <i className="fas fa-user"></i>
        </li>
      </StyledMenu>
    </StyledHeader>
  );
};
