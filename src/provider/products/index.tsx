import {
  createContext,
  useContext,
  useState,
  ReactNode,
  useEffect,
} from "react";
import toast from "react-hot-toast";
import api from "../../services/api";

interface Product {
  name: string;
  price: number;
  description: string;
  category: string;
  image: string;
  id: number;
}

interface ProductProviderProps {
  children: ReactNode;
}

interface ProductProviderData {
  products: Product[];
  cart: Product[];
  addToCart: (product: Product) => void;
  removeFromCart: (product: Product) => void;
}

const ProductsContext = createContext<ProductProviderData>(
  {} as ProductProviderData
);

export const ProductsProvider = ({ children }: ProductProviderProps) => {
  const [products, setProducts] = useState<Product[]>([]);
  const [cart, setCart] = useState<Product[]>([]);
  const cartLocal = localStorage.getItem("cart") || "null";
  const cartParse = JSON.parse(cartLocal);

  const getProducts = async () => {
    try {
      const response = await api.get("/products");
      setProducts(response.data);
    } catch (e) {
      console.log(e);
    }
  };
  useEffect(() => {
    getProducts();
  }, []);

  const addToCart = (product: Product) => {
    setCart([...cart, product]);
    localStorage.setItem("cart", JSON.stringify([...cartParse, product]));
    if (product.category === "Lanche") {
      toast.success(`${product.category} adicionado!`);
    } else {
      toast.success(`${product.category} adicionada!`);
    }
  };

  const removeFromCart = (product: Product) => {
    const newCart = cart.filter((elm) => elm.id !== product.id);
    setCart(newCart);
    localStorage.setItem("cart", JSON.stringify([newCart]));
    if (product.category === "Lanche") {
      toast.success(`${product.category} removido`);
    } else {
      toast.success(`${product.category} removida`);
    }
  };

  return (
    <ProductsContext.Provider
      value={{ cart, products, addToCart, removeFromCart }}
    >
      {children}
    </ProductsContext.Provider>
  );
};
export const useProducts = () => useContext(ProductsContext);
