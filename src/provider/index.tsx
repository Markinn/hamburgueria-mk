import { ReactNode } from "react";
import { ProductsProvider } from "./products";

interface providerChildren {
  children: ReactNode;
}

export const Providers = ({ children }: providerChildren) => {
  return <ProductsProvider>{children}</ProductsProvider>;
};
