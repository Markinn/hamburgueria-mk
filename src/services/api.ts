import axios from "axios";

const api = axios.create({
  baseURL: "https://mvburgs.herokuapp.com/",
});

export default api;
