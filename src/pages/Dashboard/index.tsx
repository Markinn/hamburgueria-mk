import styled from "styled-components";
import { Header } from "../../components/Header";
import MainPageImage from "../../assets/images/MainPageImage.jpg";

const StyledMainDiv = styled.div`
  width: 100%;
  max-width: 100%;
  height: 500px;
  background-image: url(${MainPageImage});
  background-repeat: no-repeat;
  background-position: -350px;
  background-size: cover;
  .mainContent {
    max-width: 250px;
    color: var(--white);
    font-size: 2.5rem;
    font-weight: bold;
    margin: 0;
  }
  .secondConten {
    /* max-width: 250px; */
    color: var(--white);
    font-size: 2rem;
    font-weight: bold;
  }
`;

export const Dashboard = () => {
  return (
    <>
      <Header />
      <StyledMainDiv>
        <p className="mainContent">Servindo você desde 1982.</p>
        <p className="secondConten">
          A melhor hamburgueria do brasil agora está online.
        </p>
        <button></button>
      </StyledMainDiv>
    </>
  );
};
