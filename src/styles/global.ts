import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
    :root{
        --white: #ffffff;
        --black: #000000;
        --blue: #14213D;
        --red: #DE3C2E;
        --font-red-rose: 'Red Rose', cursive;
        --font-redressed: 'Redressed', cursive;
        --font-reem-kufi: 'Reem Kufi', sans-serif;
        --font-roboto-mono: 'Roboto Mono', monospace;
        --font-roboto-slab: 'Roboto Slab', serif;
    }
    body, div, ul, button, p, img, nav, a, input, select{
    margin: 0;
    padding: 0;
    list-style-type: none;
    box-sizing: border-box;
    text-decoration: none;
    outline: none;
    }
    h1 {
        font-family: var(--font-roboto-slab);
        margin: 0;
    }
    h2{
       font-family: var(--font-redressed) 
    }
    button{
        cursor: pointer;
        font-family: var(--font-roboto-mono)
    }
`;

export default GlobalStyle;
