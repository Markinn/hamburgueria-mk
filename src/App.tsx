import { Toaster } from "react-hot-toast";
import { BrowserRouter } from "react-router-dom";
import { Providers } from "./provider";
import Router from "./routes";
import GlobalStyle from "./styles/global";

function App() {
  return (
    <>
      <Toaster />
      <BrowserRouter>
        <GlobalStyle />
        <Providers>
          <Router />
        </Providers>
      </BrowserRouter>
    </>
  );
}

export default App;
